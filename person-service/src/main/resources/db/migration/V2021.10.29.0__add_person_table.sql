CREATE TABLE public.person (
  id BIGINT CONSTRAINT person_pk_id PRIMARY KEY,
  username VARCHAR(255) NOT NULL CONSTRAINT person_unique_username UNIQUE,
  first_name VARCHAR(255) NOT NULL,
  last_name VARCHAR(255) NOT NULL
);

CREATE SEQUENCE person_seq_id OWNED BY public.person.id INCREMENT BY 1;