package de.turing85.entity.mapper;

import de.turing85.boundary.http.person.request.CreatePersonRequest;
import de.turing85.boundary.http.person.response.PersonResponse;
import de.turing85.entity.Person;
import org.mapstruct.Mapper;

@Mapper(componentModel = "cdi")
public interface PersonMapper {
  Person requestToEntity(CreatePersonRequest request);
  PersonResponse entityToResponse(Person entity);
  CreatePersonRequest responseToRequest(PersonResponse response);
}
