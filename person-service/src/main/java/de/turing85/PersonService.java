package de.turing85;

import de.turing85.boundary.client.order.OrderClient;
import de.turing85.boundary.http.person.request.CreatePersonRequest;
import de.turing85.boundary.http.person.response.PersonResponse;
import de.turing85.boundary.persistence.PersonRepository;
import de.turing85.entity.mapper.PersonMapper;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import org.eclipse.microprofile.opentracing.Traced;
import org.eclipse.microprofile.rest.client.inject.RestClient;

@ApplicationScoped
@Transactional
@Traced
public class PersonService {

  private final PersonMapper mapper;
  private final PersonRepository repository;
  private final OrderClient orderClient;

  public PersonService(
      PersonMapper mapper,
      PersonRepository repository,
      @RestClient OrderClient orderClient) {
    this.mapper = mapper;
    this.repository = repository;
    this.orderClient = orderClient;
  }

  public PersonResponse create(CreatePersonRequest request) {
    return Optional.ofNullable(request)
        .map(mapper::requestToEntity)
        .map(repository::save)
        .map(mapper::entityToResponse)
        .map(this::sendToOrderClient)
        .orElseThrow(IllegalArgumentException::new);
  }

  private PersonResponse sendToOrderClient(PersonResponse response) {
    orderClient.createPerson(mapper.responseToRequest(response));
    return response;
  }

  public Collection<PersonResponse> findAll() {
    return repository.findAll().stream()
        .map(mapper::entityToResponse)
        .collect(Collectors.toList());
  }

  public Optional<PersonResponse> findByUsername(String username) {
    return repository.findByUsername(username)
        .map(mapper::entityToResponse);
  }

  public void deleteByUsername(String username) {
    repository.deleteByUsername(username);
  }
}
