package de.turing85.boundary.client.order;

import de.turing85.boundary.http.person.request.CreatePersonRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@RegisterRestClient(configKey = "order-client")
@Path("persons")
public interface OrderClient {

  @POST
  void createPerson(CreatePersonRequest request);
}
