package de.turing85.boundary.http.person.response;

import lombok.AllArgsConstructor;
import lombok.Value;

import javax.validation.constraints.NotNull;

@Value
@AllArgsConstructor
public class PersonResponse {

  @NotNull
  String username;

  @NotNull
  String firstName;

  @NotNull
  String lastName;
}
