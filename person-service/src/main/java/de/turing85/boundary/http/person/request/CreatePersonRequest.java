package de.turing85.boundary.http.person.request;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class CreatePersonRequest {

  @NotNull
  String username;

  @NotNull
  String firstName;

  @NotNull
  String lastName;
}
