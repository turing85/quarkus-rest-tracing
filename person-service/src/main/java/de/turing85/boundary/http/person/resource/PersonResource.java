package de.turing85.boundary.http.person.resource;

import de.turing85.PersonService;
import de.turing85.boundary.http.person.request.CreatePersonRequest;
import de.turing85.boundary.http.person.response.PersonResponse;
import java.net.URI;
import java.util.NoSuchElementException;
import javax.validation.Valid;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import lombok.RequiredArgsConstructor;

@Path("persons")
@RequiredArgsConstructor
public class PersonResource {

  private final PersonService service;

  @GET
  public Response getAllPersons() {
    return Response.ok(service.findAll()).build();
  }

  @POST
  public Response postPerson(@Valid CreatePersonRequest request) {
    final PersonResponse response = service.create(request);
    return Response
        .created(URI.create(String.format("/persons/%s", response.getUsername())))
        .entity(response)
        .build();
  }

  @GET
  @Path("{username}")
  public Response getByUsername(@PathParam("username") String username) {
    return service.findByUsername(username)
        .map(Response::ok)
        .map(ResponseBuilder::build)
        .orElseThrow(NoSuchElementException::new);
  }

  @DELETE
  @Path("{username}")
  public Response deleteByUsername(@PathParam("username") String username) {
    service.deleteByUsername(username);
    return Response.noContent().build();
  }
}
