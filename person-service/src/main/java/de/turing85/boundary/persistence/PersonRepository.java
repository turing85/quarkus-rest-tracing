package de.turing85.boundary.persistence;

import de.turing85.entity.Person;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Long> {

  Optional<Person> findByUsername(String username);

  void deleteByUsername(String username);
}
