package de.turing85.boundary.http.person.request;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class CreatePersonRequest {
  String username;
}
