package de.turing85.boundary.http.person.resource;

import de.turing85.PersonService;
import de.turing85.boundary.http.person.request.CreatePersonRequest;
import de.turing85.boundary.http.person.response.PersonResponse;
import java.net.URI;
import javax.enterprise.context.ApplicationScoped;
import javax.validation.Valid;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import lombok.RequiredArgsConstructor;

@ApplicationScoped
@Path("persons")
@RequiredArgsConstructor
public class PersonResource {

  private final PersonService service;

  @POST
  public Response postPerson(@Valid CreatePersonRequest request) {
    final PersonResponse response = service.createPerson(request);
    return Response
        .created(URI.create(String.format("/persons/%s", response.getUsername())))
        .entity(response)
        .build();
  }
}
