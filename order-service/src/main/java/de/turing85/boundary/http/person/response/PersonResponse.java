package de.turing85.boundary.http.person.response;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class PersonResponse {
  String username;
}
