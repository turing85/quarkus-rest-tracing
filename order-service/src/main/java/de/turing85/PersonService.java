package de.turing85;

import de.turing85.boundary.persistence.PersonRepository;
import de.turing85.boundary.http.person.request.CreatePersonRequest;
import de.turing85.boundary.http.person.response.PersonResponse;
import de.turing85.entity.mapper.PersonMapper;
import java.util.Optional;
import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.eclipse.microprofile.opentracing.Traced;

@ApplicationScoped
@Transactional
@Traced
@RequiredArgsConstructor
public class PersonService {

  private final PersonMapper mapper;
  private final PersonRepository repository;

  public PersonResponse createPerson(CreatePersonRequest request) {
    return Optional.of(request)
        .map(mapper::requestToEntity)
        .map(repository::save)
        .map(mapper::entityToResponse)
        .orElseThrow(IllegalArgumentException::new);
  }
}
