CREATE TABLE public.person (
  id BIGINT CONSTRAINT person_pk_id PRIMARY KEY,
  username VARCHAR(255) NOT NULL CONSTRAINT person_unique_username UNIQUE
);

CREATE SEQUENCE person_seq_id OWNED BY public.person.id INCREMENT BY 1;