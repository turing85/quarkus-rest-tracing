# Testing OpenTracing with REST client
In this project, we have two service:

- the [person service](./person-service), handling persons
- the [order service](./order-service), handling orders

and we assume that the order service needs some knowledge about the created persons in order to 
operate. For this, we want that the person service sends some information to the order service, 
whenever a new person is created. We want to send this information via HTTP/REST. We also want to 
make this communication visible through opentracing.

## Start dependencies
We begin by starting all necessary dependencies (a postgres-database, a pgadmin4 web client and a 
jaeger service) by executing

```shell
cd localdeployment
docker-compose up -d
cd ..
```
## Start services
Next, we start both services. We need two terminals to do so. In one terminal, we execute
```bash
./mvnw --projects order-service quarkus:dev
```

In another terminal, we execute
```bash
./mvnw --projects person-service quarkus:dev
```

We wait until both services are started. The services are started when we see a log message similar 
to this one:

```
2021-10-30 14:06:50,774 INFO  [io.quarkus] (Quarkus Main Thread) ... started in 7.262s. ...
```

## Making some requests
Both services include a swagger-ui. For our use case, however, we only need the swagger-ui of the 
person service. We access [`http://localhost:8080/q/swagger-ui`](http://localhost:8080/q/swagger-ui),
click on the `POST` endpoint, then on `Try it out`, and then `Execute`. In the response, we should
see data similar to this: 
```
{
  "username": "string",
  "firstName": "string",
  "lastName": "string"
}
```

## Analyzing the traces
Now that we created a new person, we expect the person service called an endpoint on 
the order service. To validate this, we are going to access jaeger on 
[`http://localhost:16686`](http://localhost:16686). On the left under `Services`, we select 
`persons-service` (if it is not already selected) and then click on `Find Traces`. We should see one
trace with nine spans:

![jaeger-overview](./images/jaeger-overview.png)

When we click on the trace, we see a detailed graph of this trace with all its spans:

![jaeger-details](./images/jaeger-detailpng.png)

We can not only see the communication from the person service to the order service, we can also see
the database queries in both services.

If we so desire, we can even drill down to inspect a single query be clicking (and therefore 
expanding) a DB query trace:

![jaeger-trace-dbquery](./images/jaeger-trace-dbquery.png)

The beauty of this that we only had to configure some jaeger properties and database tracing (see
[the Quarkus guide on OpenTracing](https://quarkus.io/guides/opentracing)). Tracing over the 
rest-call (which is realized through `quarkus-rest-client`, see 
[the Quarkus guide on using the REST Client](https://quarkus.io/guides/rest-client)) "just works" 
without additional configuration or code modification.